//App definition and dependencies
angular
	.module('app',[
	'ngRoute',
	'ngResource',
	'mgcrea.ngStrap',
	'ngMockE2E',
	'ngSanitize'
]);
