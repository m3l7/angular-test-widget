// Log HTTP requests to browser's console

angular
	.module('app')
	.factory('httpInterceptor',httpInterceptor);
angular
	.module('app')
	.config(configInterceptor);


function httpInterceptor($log,config){
	return {
		request: function(req){
			if (config.logging.http){
				$log.info(req.method+": "+req.url);	
				if (!!req.data) $log.log(JSON.stringify(req.data));
			} 
			return req;
		}
	};
}

function configInterceptor($httpProvider){
	$httpProvider.interceptors.push('httpInterceptor');
}
