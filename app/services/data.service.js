//Data (model) layer
//TODO: error management

angular
	.module('app')
	.factory('dataservice',dataservice);

function dataservice($q,$log,apiservice){

	// models object
		var file = {
			file: '',
			get: function(){return file.file;},
			fetch: fetchFile
		};
		var textEditor = {
			text: '',
			get: function(){return textEditor.text;},
			fetch: fetchTextEditor,
			set: function(text){if (!!text) textEditor.text=text;},
			save: saveTextEditor
		};
		var questions = {
			list: [],
			fetch: fetchQuestions,
			add: addQuestion
		};

	return {
		file: file,
		textEditor: textEditor,
		questions: questions
	};

	// File methods
		function fetchFile(){
			return apiservice.file.get({}).$promise
				.then(fetchFileComplete)
				.catch(requestError);

			function fetchFileComplete(response){
				if ((!!response) && (!!response.content)){
					file.file = response.content;
					return response.content;
				}
				else return $q.reject({data:'Fetch file: Wrong response'});
			}
		}

	//Text editor methods
		function saveTextEditor(text){
			if (!!text){
				return apiservice.textEditor.save({content:text}).$promise
					.then(requestTextEditorComplete)
					.catch(requestError);			
			}
			else return $q.reject({data:'Save text Editor: Wrong text'})
		}

		function fetchTextEditor(){
			return apiservice.textEditor.get({}).$promise
				.then(requestTextEditorComplete)
				.catch(requestError);			
		}

		function requestTextEditorComplete(response){
			if ((!!response) && (!!response.content)){
				textEditor.text = response.content;
				return response.content;
			}
			else return $q.reject({data:'Fetch text editor: Wrong response'});
		}

	// Questions methods
		function fetchQuestions(){
			return apiservice.questions.query({}).$promise
				.then(requestQuestionsComplete)
				.catch(requestError);
		}
		function addQuestion(){
			var newQuestion = new apiservice.questions();
			return newQuestion.$save()
				.then(validateQuestion)
				.then(function(question){
					questions.list.push(question);
				})
				.catch(requestError);
		}

		function requestQuestionsComplete(response){
			if (!!response){
				if (response.length) {
					for (var i in response) validateQuestion(response[i]);
					questions.list = response;
				}
				return response;
			}
			else return $q.reject({data:'Fetch questions: Wrong response'});
		}
		function validateQuestion(question){
			if (!!question){
				if (!question.rating) question.rating = 0;
				if (!question.answer) question.answer = '';
				if (!question.question_text) question.question_text = '';
				return question;
			}
			else return $q.reject({data:'Validate Question: Wrong response'});
		}

	// Error management
		function requestError(err){
			if ((!!err) && (!!err.data)){
				if (!!err.data.error_details) $log.error('Request error: '+err.data.error_details);	
				else $log.error('Request error: '+err.data);	
			} 
		}
}
