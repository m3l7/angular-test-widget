angular
	.module('app')
	.factory('apiservice',apiservice);

function apiservice($resource){
	return {
		file: $resource('dashboard/1/file'),
		textEditor: $resource('dashboard/1/notes'),
		questions: $resource('dashboard/1/questions/:questionId',{questionId:'@id'})
	};
};
