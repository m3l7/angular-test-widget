angular.module('app')
    .directive('richTextEditor', function( $timeout, dataservice, config) {
 
        var self = this;
        var directive = {
                restrict : "A",
                replace : true,
                transclude : true,
                scope : {},             
                template : 
                        "<div>" +
                                "<textarea id=\"richtexteditor-content\" style=\"height:"+config.textEditor.height+";width:"+config.textEditor.width+"\"></textarea>"+
                        "</div>",
 
                link : function( $scope, $element, $attrs ) {

                        var textarea = $('#richtexteditor-content').wysihtml5();
                        var editor = textarea.data('wysihtml5').editor;

                        // fetch initial text from server
                            dataservice.textEditor.fetch()
                                .then(function(text){
                                    if (!!text) editor.setValue(text);
                                })

                        // events
                            editor.observe('newword:composer', onChange);

                        // save to server
                            $scope.$parent.texteditor.saving = false;
                            $scope.$parent.texteditor.saveQueue = false;
                            function onChange(){
                                if (!$scope.$parent.texteditor.saving){
                                    dataservice.textEditor.save($('#richtexteditor-content').val());
                                    $scope.$parent.texteditor.saving = true;
                                    $scope.$parent.texteditor.saveQueue = false;
                                    $timeout(function(){
                                        $scope.$parent.texteditor.saving = false;
                                        if ($scope.$parent.texteditor.saveQueue) onChange();
                                    },config.textEditor.saveDelay);
                                }
                                else {
                                    $timeout(function(){
                                        $scope.$parent.texteditor.saveQueue = true;
                                    });
                                }
                            };
                }
        }
        return directive;
});