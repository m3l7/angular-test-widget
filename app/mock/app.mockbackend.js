// Backend mocking with fake data

angular
	.module('app')
	.run(mockBackend);

function mockBackend($httpBackend,testData,config){
	
	// File
		$httpBackend.whenGET('dashboard/1/file').respond(function(method,url,data){
			if (config.file.disableJS) testData.doc = testData.doc.replace('script','_script');
			return [200,{content: testData.doc},{}];
		})

	// Text Editor
		$httpBackend.whenGET(/dashboard\/1\/notes.*/).respond(function(method,url,data){
			return [200,{content: testData.textEditor},{}];
		})
		$httpBackend.whenPOST(/dashboard\/1\/notes.*/).respond(function(method,url,data){
			if ((!!data) && (!!JSON.parse(data).content)){
				var content = JSON.parse(data).content;
				return [200,{content: content},{}];
			}
			else return [400,{error_details:'content field is required'},{}];
		})

	// Questions
		$httpBackend.whenGET(/dashboard\/1\/questions$/).respond(function(method,url,data){
			return [200,testData.questions,{}];
		})
		$httpBackend.whenPOST(/dashboard\/1\/questions$/).respond(function(method,url,data){
			var id = testData.questions.length+1;
			testData.questions.push({id:id});
			return [200,{id:id},{}];
		})
		$httpBackend.whenPOST(/dashboard\/1\/questions\/([0-9]+)/).respond(function(method,url,data){
			if (!!data) {
				var index = 0;
				var data = JSON.parse(data);
				for (var i in testData.questions) if (testData.questions[i].id==data.id) index = i;
				testData.questions[index].answer = data.answer;
				testData.questions[index].rating = data.rating;
				return [200,data,{}];
			}
			else return [400,{error_details:'content field is required'},{}];
		})

	// Default
		$httpBackend.whenGET(/.*/).passThrough();
		$httpBackend.whenPOST(/.*/).passThrough();
}
