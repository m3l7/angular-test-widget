//global configuration

var config = {
	textEditor: {
		saveDelay : 3000, //minimum delay in ms between two save request
		height: "90%",
		width: "100%"
	},
	questions:{
		saveDelay : 2000 //minimum delay in ms between two save request
	},
	file:{
		disableJS: true //remove javascript form file's html to avoid bad errors and security issues
	},
	logging:{
		http: true
	}
};

angular
	.module('app')
	.constant('config',config) //config for the text editor
	.config(function($sceProvider){
		$sceProvider.enabled(false);
	})
