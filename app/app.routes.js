//Main routes

angular
	.module('app')
	.config(function($routeProvider) {
	    
	    $routeProvider
	      .when('/file', {
	        templateUrl: 'tabs/file.html', 
	        controller: 'File',
	        controllerAs: 'file'
	      })
	      .when('/questions', {
	        templateUrl: 'tabs/questions.html', 
	        controller: 'Questions',
	        controllerAs: 'questions'
	      })
	      .otherwise({redirectTo: '/file'})

	});
