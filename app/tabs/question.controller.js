angular
	.module('app')
	.controller('Questions',questions);

function questions($timeout,config,dataservice){
	var vm = this;

	vm.questionsData = dataservice.questions;
	vm.setRating = setRating;
	vm.onAnswerChange = onAnswerChange;
	vm.index = 0;

	initialize();

	function initialize(){
		dataservice.questions.fetch()
	}
	function setRating(question_index,rating){
		if ((!!rating) && (!!dataservice.questions.list[question_index])) {
			dataservice.questions.list[question_index].rating = rating;
			dataservice.questions.list[question_index].$save();
		}
	}

	// Save answers when they are changed.
	// 	'saving' is to avoid to save too often
	// 	'toSave' is a save queue
	var saving = false;
	var toSave = false;
	function onAnswerChange(question_index){
		if (!!dataservice.questions.list[question_index]){
			if (!saving) {
				dataservice.questions.list[question_index].$save();
				saving = true;
				toSave = false;
				$timeout(function(){
					saving=false;
					if (toSave) onAnswerChange(question_index);
				},config.questions.saveDelay)
			}
			else toSave = true;
		}
	}
}
