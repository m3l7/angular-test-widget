angular
	.module('app')
	.controller('File',File);

function File(dataservice){
	var vm = this;

	vm.fileData = dataservice.file;

	initialize();

	function initialize(){
		dataservice.file.fetch();
	};
}
