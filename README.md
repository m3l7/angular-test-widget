angular_widget
==============

Install
-------

Clone the repo and install dependencies with bower:

    bower install
    
Run
---

Run app/index.html directly from a browser.
Remember to set the browser to allow local (file) XHR requests.

Otherwise, you can run server.js if you have node and express installed, and open http://localhost:8999/app

Config
------

Edit app/app.config.js 

Debugging
---------

At any time, you can see the mock backend data by opening you browser's console and inspect 'testData' object